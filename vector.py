import numpy as np
import math

class Vector:

    def __init__(self, *values):
        if isinstance(values[0], type(np.array([]))):
            self.vals = values[0]

        # if a str(np.array) is passed, this makes it into a normal np.array
        elif isinstance(values[0], str):
            st = str(values[0])
            st = st[:-1]
            st = st[1:]
            st = list(st.split(' '))
            st = [int(x) for x in st]
            self.vals = np.array(st)
        else:
            self.vals = np.array(values)


    def magnitude(self):
        tot = 0
        for i in self.vals:
            tot = tot + i ** 2
        tot = tot ** .5
        return round(tot, 4)


    def normal(self):
        return Vector(self.vals / self.magnitude())


    # note rads and degs are from closest x axis, fix later
    def direction(self):
        rad = round(math.atan(self.vals[1]/self.vals[0]), 4)
        deg = round(math.degrees(rad), 4)
        return f'atan({self.getVal(1)}/{self.getVal(0)})\n - Radians: {rad}\n - Degrees: {deg}'


    def getVal(self, index):
        return round(self.vals[index], 4)


    def shift(self, index, units):
        self.vals[index] = self.vals[index] + units


    def __str__(self):
        return str(self.vals)


    def __add__(self, other):
        # type(other) is type(np.array([])):
        if isinstance(other, type(np.array([]))):
            return Vector(self.vals + other)
        else:
            return Vector(self.vals + other.vals)


    def __mul__(self, other):
        if isinstance(other, type(Vector)):
            return Vector(self.vals * other.vals)

        else:
            return Vector(self.vals * other)







# backup
# class Vector:
#
#     def __init__(self, *values):
#         if str(type(values[0])) == "<class 'numpy.ndarray'>":
#             self.vals = np.array(values[0])
#
#         elif str(type(values[0])) == "<class 'str'>":
#
#             st = str(values[0])
#             st = st[:-1]
#             st = st[1:]
#             st = list(st.split(' '))
#             st = [int(x) for x in st]
#             self.vals = np.array(st)
#         else:
#             self.vals = np.array(values)
#
#
#
#
#
#     def magnitude(self):
#         tot = 0
#         for i in self.vals:
#             tot = tot + i ** 2
#         tot = tot ** .5
#         return round(tot, 4)
#
#
#     def normal(self):
#         return Vector(self.vals / self.magnitude())
#
#
#     def direction(self):
#         rad = round(math.atan(self.vals[1]/self.vals[0]), 4)
#         deg = round(math.degrees(rad),4)
#         return f'atan({self.getVal(1)}/{self.getVal(0)})\n= {rad} - Radians\n= {deg} - Degrees'
#
#
#     def getVal(self, index):
#         return round(self.vals[index], 4)
#
#
#     def shift(self, index, units):
#         self.vals[index] = self.vals[index] + units
#
#
#     def __str__(self):
#         return str(self.vals)
#
#
#     def __add__(self, other):
#         if str(type(other)) == "<class 'numpy.ndarray'>":
#             return Vector(self.vals + other)
#         else:
#             return Vector(self.vals + other.vals)
#
#
#     def __mul__(self, other):
#         s = str(type(other))
#         if s == "<class 'helpers.vector.Vector'>":
#             return Vector(self.vals * other.vals)
#
#         else:
#             return Vector(self.vals * other)
